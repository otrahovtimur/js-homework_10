// ТЕОРІЯ
/*
1.
Способи для сворення елементу:
document.createElement();
document.createTextNode();

2.
const nav = document.querySelector('.navigation');
Вибір елемента для видалення
const elNavigation = nav[i];
Видалення елемента зі сторінки
elNavigation.parentNode.removeChild(elNavigation);

3.
element.before(node,strings)
element.after(node,strings)
element.append(node,strings)
element.prepend(node,strings)
insertAdjacentHTML, Element, Text (beforebegin, afterbegin, beforeend, afterend);

*/

//  ПРАКТИКА

//1.
const link = document.createElement('a');
link.textContent = 'Learn More';
link.href = '#';
const footer = document.querySelector('footer');
footer.append(link);

//2.
const main = document.querySelector('main');
const newSelect = document.createElement('select');
newSelect.id = 'rating';
main.prepend(newSelect);

const options = [
    { value: "4", text: "4 Stars" },
    { value: "3", text: "3 Stars" },
    { value: "2", text: "2 Stars" },
    { value: "1", text: "1 Star" }
  ];
  
  options.forEach( option => {
    const newOption = document.createElement("option");
    newOption.value = option.value;
    newOption.textContent = option.text;
    newSelect.append(newOption);
});

  


